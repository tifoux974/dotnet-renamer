﻿Imports DotNetRenamer.Implementer.Analyzer
Imports DotNetRenamer.Implementer.Context
Imports DotNetRenamer.XertzLoginTheme

Public Class Frm_Main

#Region " Variables "
    Private WithEvents _param As Cls_Parameters
    Private WithEvents _Context As Cls_Context
    Private _paramArgs As Cls_RenamerState
    Private _controlList As List(Of Control)
    Private _taskIsRunning As Boolean
    Private _LanguageType%
    Private _NRenamed, _TRenamed, _MRenamed, _PRenamed, _GRenamed, _ERenamed, _FRenamed, _PaRenamed, _VRenamed As Integer
#End Region

#Region " Initialize "
    Public Sub New()
        InitializeComponent()
        _paramArgs = New Cls_RenamerState
    End Sub
#End Region

#Region " Frm_main "
    Private Sub Frm_Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If _taskIsRunning Then
            MessageBox.Show("Please wait while renaming !", "Wait ...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            e.Cancel = True
        End If
    End Sub
#End Region

#Region " Select Assembly "
    Private Sub BtnSelectFile_Click(sender As Object, e As EventArgs) Handles BtnSelectFile.Click
        Dim ofd As New OpenFileDialog
        With ofd
            .Title = "Select a DotNet program (VbNet, C#)"
            .Filter = "Exe|*.exe;*.exe"
            .CheckFileExists = True
            .Multiselect = False
            If .ShowDialog() = DialogResult.OK Then
                Me.ShowSelectedFileInfos(.FileName)
            End If
        End With
    End Sub

    Private Sub ShowSelectedFileInfos(FilePath$)
        Try
            _param = New Cls_Parameters(FilePath, FilePath & "-Renamed.exe")
            If _param.isValidFile Then
                TxbAssemblyInfo.Text = _param.getAssemblyName
                TxbVersionInfo.Text = _param.getAssemblyVersion
                TxbType.Text = _param.getModuleKind
                TxbFrameworkInfo.Text = _param.getRuntime
                TxbCpuTargetInfo.Text = _param.getProcessArchitecture
                TxbSelectedFile.Text = FilePath
                PbxSelectedFile.Image = System.Drawing.Icon.ExtractAssociatedIcon(FilePath).ToBitmap
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub OnFileValidationTeminated(sender As Object, e As Cls_ValidatedFile) Handles _param.FileValidated
        If e.isValid Then
            GbxAsemblyInfos.Enabled = True
            GbxPresets.Enabled = True
            BtnStart.Enabled = True
        Else
            GbxAsemblyInfos.Enabled = False
            GbxPresets.Enabled = False
            EmptyTextBox()
        End If
    End Sub

    Private Sub EmptyTextBox()
        TxbAssemblyInfo.Text = String.Empty
        TxbVersionInfo.Text = String.Empty
        TxbType.Text = String.Empty
        TxbFrameworkInfo.Text = String.Empty
        TxbCpuTargetInfo.Text = String.Empty
        TxbSelectedFile.Text = String.Empty
        PbxSelectedFile.Image = Nothing
    End Sub
#End Region

#Region " Select Presets "
    Private Sub ChbNamespacesRP_CheckedChanged(sender As Object) Handles ChbNamespacesRP.CheckedChanged
        If ChbNamespacesRP.Checked Then
            PnlNamespacesGroup.Enabled = True
        Else
            PnlNamespacesGroup.Enabled = False
        End If
    End Sub

    Private Sub RdbAlphabetic_CheckedChanged(sender As Object, e As EventArgs) Handles RdbJapaneseCharacters.CheckedChanged, RdbInvisibleCharacters.CheckedChanged, RdbDotsCharacters.CheckedChanged, RdbChineseCharacters.CheckedChanged, RdbAlphabeticCharacters.CheckedChanged
        Dim rdb As LogInRadioButton = TryCast(sender, LogInRadioButton)
        If rdb.Checked Then _LanguageType = rdb.TabIndex
    End Sub

    Private Sub CbxPresets_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxPresets.SelectedIndexChanged
        If Not _controlList Is Nothing Then _controlList.Clear()
        _controlList = New List(Of Control) From { _
                                            {RdbAlphabeticCharacters}, {RdbDotsCharacters}, {RdbInvisibleCharacters}, _
                                            {RdbChineseCharacters}, {RdbJapaneseCharacters}, {ChbRenameMainNamespaceOnlyNamespaces}, _
                                            {ChbReplaceNamespaceByEmptyNamespaces}, {ChbNamespacesRP}, {ChbTypesRP}, {ChbMethodsRP}, _
                                            {ChbPropertiesRP}, {ChbEventsRP}, {ChbFieldsRP}, {ChbAttributesRP}, {ChbParametersRP}}
        Select Case CbxPresets.SelectedIndex
            Case 0
                EnabledPresets(False)
                _paramArgs.RenameRuleSetting = Cls_RenamerState.RenameRule.Full
                SetPreset(0, _controlList)
                LblPresets.Visible = True
            Case 1
                EnabledPresets(False)
                _paramArgs.RenameRuleSetting = Cls_RenamerState.RenameRule.Medium
                SetPreset(1, _controlList)
                LblPresets.Visible = True
            Case 2
                EnabledPresets(True)
                _paramArgs.RenameRuleSetting = Cls_RenamerState.RenameRule.Personalize
                LblPresets.Visible = False
                If ChbNamespacesRP.Checked = True Then PnlNamespacesGroup.Enabled = True
        End Select
    End Sub

    Private Sub SetPreset(Rule%, controlList As List(Of Control))
        For Each c In controlList
            If TypeOf c Is LogInRadioButton Then
                If c.Name.EndsWith("Characters") Then
                    If Rule = 0 Then
                        If c.Tag = 0 Then
                            TryCast(c, LogInRadioButton).Checked = True
                        End If
                    ElseIf Rule = 1 Then
                        If c.Tag = 4 Then
                            TryCast(c, LogInRadioButton).Checked = True
                        End If
                    End If
                End If
            ElseIf TypeOf c Is LogInCheckBox Then
                If c.Name.EndsWith("Namespaces") Then
                    If Rule = 0 Then
                        If c.Tag = 0 Then
                            TryCast(c, LogInCheckBox).Checked = False
                        ElseIf c.Tag = 1 Then
                            TryCast(c, LogInCheckBox).Checked = True
                        End If
                    ElseIf Rule = 1 Then
                        TryCast(c, LogInCheckBox).Checked = False
                    End If
                ElseIf c.Name.EndsWith("RP") Then
                    If Rule = 0 Then
                        TryCast(c, LogInCheckBox).Checked = True
                    ElseIf Rule = 1 Then
                        Select Case c.Tag.ToString
                            Case "Namespaces", "Types", "Methods", "Properties", "Attributes"
                                TryCast(c, LogInCheckBox).Checked = True
                                Exit Select
                            Case "Events", "Fields", "Parameters"
                                TryCast(c, LogInCheckBox).Checked = False
                                Exit Select
                        End Select
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub EnabledPresets(state As Boolean)
        PnlCharactersPresets.Enabled = state
        PnlNamespacesPresets.Enabled = state
        PnlTypesPresets.Enabled = state
        PnlMethodsPresets.Enabled = state
        PnlPropertiesPresets.Enabled = state
        PnlEventsPresets.Enabled = state
        PnlFieldsPresets.Enabled = state
        PnlAttributesPresets.Enabled = state
        PnlParametersPresets.Enabled = state
    End Sub
#End Region

#Region " Rename Task "

    Private Sub BtnStart_Click(sender As Object, e As EventArgs) Handles BtnStart.Click
        If Not BgwRenameTask.IsBusy Then
            LsbMain.ShowLine = True
            BtnStart.Enabled = False
            _taskIsRunning = True
            EnabledControls(False)
            BgwRenameTask.RunWorkerAsync(CbxPresets.SelectedIndex)
        End If
    End Sub

    Private Sub EnabledControls(state As Boolean)
        GbxAsemblyInfos.Enabled = state
        GbxSelectFile.Enabled = state
        GbxAsemblyInfos.Enabled = state
        GbxPresets.Enabled = state
    End Sub

    Private Sub BgwRenameTask_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BgwRenameTask.DoWork
        Try
            If CInt(e.Argument) = 0 OrElse CInt(e.Argument) = 1 Then
                _param.RenamingAccept = _paramArgs
            Else
                With _paramArgs
                    .Namespaces = ChbNamespacesRP.Checked
                    .Types = ChbTypesRP.Checked
                    .Methods = ChbMethodsRP.Checked
                    .Properties = ChbPropertiesRP.Checked
                    .CustomAttributes = ChbAttributesRP.Checked
                    .Events = ChbEventsRP.Checked
                    .Fields = ChbFieldsRP.Checked
                    .Parameters = ChbParametersRP.Checked
                    .Variables = ChbParametersRP.Checked
                    .RenameMainNamespaceSetting = ChbRenameMainNamespaceOnlyNamespaces.Checked
                    .ReplaceNamespacesSetting = ChbReplaceNamespaceByEmptyNamespaces.Checked
                End With
                _paramArgs.RenamingType = _LanguageType
                _param.RenamingAccept = _paramArgs
            End If

            _Context = New Cls_Context(_param)
            With _Context
                .ReadAssembly()
                .RenameAssembly()
                .WriteAssembly()
                .Clean()
            End With    

            e.Result = New Object() {"Success", "Completed"}
        Catch ex As Exception
            e.Result = New Object() {"Error", ex.ToString}
        End Try

    End Sub

    Private Sub OnRenamedItem(sender As Object, e As Cls_RenamedItemEventArgs) Handles _Context.RenamedItem
        'MsgBox(e.item.ItemType & " : From --> " & e.item.ItemName & " : To --> " & e.item.obfuscatedItemName)
        Select Case e.item.ItemType
            Case "Namespace"
                _NRenamed += 1
            Case "Type"
                _TRenamed += 1
            Case "Method"
                _MRenamed += 1
            Case "Parameter"
                _PaRenamed += 1
            Case "Generic Parameter"
                _GRenamed += 1
            Case "Variable"
                _VRenamed += 1
            Case "Property"
                _PRenamed += 1
            Case "Event"
                _ERenamed += 1
            Case "Field"
                _FRenamed += 1
        End Select
    End Sub

    Private Sub BgwRenameTask_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwRenameTask.RunWorkerCompleted
        If Not e.Result Is Nothing Then
            Select Case e.Result(0)
                Case "Error"
                    MessageBox.Show(e.Result(1).ToString, e.Result(0), MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Select
                Case "Success"
                    MessageBox.Show("Renamed :" & vbNewLine & vbNewLine & "Namespace(s) : " & _NRenamed.ToString & vbNewLine & _
                                              "Type(s) : " & _TRenamed.ToString & vbNewLine & _
                                              "Method(s) : " & _MRenamed.ToString & vbNewLine & _
                                              "Parameter(s) : " & _PaRenamed.ToString & vbNewLine & _
                                              "Generic Parameter(s) : " & _GRenamed.ToString & vbNewLine & _
                                              "Variable(s) : " & _VRenamed.ToString & vbNewLine & _
                                              "Property(s) : " & _PRenamed.ToString & vbNewLine & _
                                              "Event(s) : " & _ERenamed.ToString & vbNewLine & _
                                              "Field(s) : " & _FRenamed.ToString _
                    , "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Select
            End Select
        End If
        _taskIsRunning = False
        EmptyTextBox()
        GbxSelectFile.Enabled = True
        GbxAsemblyInfos.Enabled = True
        CbxPresets.SelectedIndex = 0
        LsbMain.ShowLine = False
        ClearIncrement()
    End Sub

    Private Sub ClearIncrement()
        _NRenamed = 0
        _TRenamed = 0
        _MRenamed = 0
        _PaRenamed = 0
        _GRenamed = 0
        _VRenamed = 0
        _PRenamed = 0
        _ERenamed = 0
        _FRenamed = 0
    End Sub
#End Region

#Region " Other "
    Private Sub LnkLblBlogSpot_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LnkLblBlogSpot.LinkClicked
        Process.Start(LnkLblBlogSpot.Text)
    End Sub
#End Region

End Class