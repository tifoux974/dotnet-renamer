﻿Imports System.Reflection
Imports DotNetRenamer.Core20Reader

Namespace Analyzer

    ''' <summary>
    ''' INFO : This is the first step of the renamer library. 
    '''        You must pass two arguments (inputFile and outputFile properties) when instantiating this class.
    '''        You can either check if the selected file if executable and DotNet by calling the isValidFile routine.
    ''' </summary>
    Public Class Cls_Analyzer

#Region " Variables "
        Private _inputFile$
        Private _outputFile$
        Private _pe As Cls_Core20Reader
        Private _assName As AssemblyName
#End Region

#Region " Events "
        Public Event FileValidated(sender As Object, e As Cls_ValidatedFile)
#End Region

#Region " Properties "
        Public ReadOnly Property inputFile As String
            Get
                Return Me._inputFile
            End Get
        End Property

        Public ReadOnly Property outputFile As String
            Get
                Return Me._outputFile
            End Get
        End Property
#End Region

#Region " Initialize "
        ''' <summary>
        ''' INFO : Initilize a new instance of the class Analyzer.Cls_Analyzer which used to check if the selected inputfile is a valid PE and executable file. 
        ''' </summary>
        ''' <param name="inputFilePath"></param>
        ''' <param name="outputFilePath"></param>
        Public Sub New(inputFilePath$, outPutFilePath$)
            _inputFile = inputFilePath
            _outputFile = outPutFilePath
            _pe = New Cls_Core20Reader()
            _pe.ReadFile(_inputFile)
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' INFO : Check if inputfile is valid DotNet and executable and not Wpf !
        ''' </summary>
        Public Function isValidFile() As Boolean
            If _pe.isExecutable Then
                If _pe.isManagedFile Then
                    If Not isWpfProgram() Then
                        _assName = Reflection.AssemblyName.GetAssemblyName(_inputFile)
                        RaiseEvent FileValidated(Me, New Cls_ValidatedFile(True))
                        Return True
                    End If
                End If
            End If
            RaiseEvent FileValidated(Me, New Cls_ValidatedFile(False))
            Return False
        End Function

        Public Function getAssemblyName() As String
            Return _assName.Name
        End Function

        Public Function getAssemblyVersion() As String
            Return _assName.Version.ToString
        End Function

        Public Function getModuleKind() As String
            Return _pe.GetSystemType
        End Function

        Public Function getRuntime() As String
            Return _pe.GetTargetRuntime
        End Function

        Public Function getProcessArchitecture() As String
            Return _pe.GetTargetPlatform
        End Function

        ''' <summary>
        ''' INFO : Check if inputfile is WPF application. 
        ''' </summary>
        ''' <remarks>
        '''        DotNet Renamer didn't take charge WPF program !
        ''' </remarks>
        Private Function isWpfProgram() As Boolean
            Dim b As Boolean = False
            For Each assName In Assembly.LoadFrom(_inputFile).GetReferencedAssemblies()
                If assName.Name.ToLower = "system.xaml" Then
                    b = True
                    Exit For
                End If
            Next
            If b Then
                For Each assName In Assembly.LoadFrom(_inputFile).GetManifestResourceNames()
                    If assName.ToLower.EndsWith(".g.resources") Then
                        Return True
                    End If
                Next
            End If
            Return False
        End Function
#End Region

    End Class
End Namespace
