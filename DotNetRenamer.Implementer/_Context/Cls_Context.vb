﻿Imports Mono.Cecil
Imports DotNetRenamer.Implementer.Processing
Imports DotNetRenamer.Helper.RandomizeHelper

Namespace Context

    ''' <summary>
    ''' INFO : This is the second step of the renamer library. 
    '''        You must pass one argument (parameter) when instantiating this class and calling the RenameAssembly routine.
    ''' </summary>
    Public Class Cls_Context

#Region " Variables "
        Public AssDef As AssemblyDefinition
        Private _parameters As Cls_Parameters
#End Region

#Region " Events "
        Public Shared Event RenamedItem As RenamedItem
#End Region

#Region " Initialize "
        ''' <summary>
        ''' INFO : Initializes a new instance of the Context.Cls_Context class which allows to add parameters such as members and types state before the task of renaming starts.
        ''' </summary>
        ''' <param name="Parameters"></param>
        Public Sub New(ByVal parameters As Cls_Parameters)
            _parameters = parameters
        End Sub
#End Region

#Region " Methods "
        ''' <summary>
        ''' INFO : Raise event when a type or a member renamed.
        ''' </summary>
        ''' <param name="it"></param>
        Public Shared Sub RaiseRenamedItemEvent(it As Cls_RenamedItem)
            Dim itemEvent As New Cls_RenamedItemEventArgs(it)
            RaiseEvent RenamedItem(Nothing, itemEvent)
            itemEvent = Nothing
        End Sub

        ''' <summary>
        ''' INFO : this routine reads the assembly. It uses Mono Cecil library.
        ''' </summary>
        Public Sub ReadAssembly()
            AssDef = AssemblyDefinition.ReadAssembly(_parameters.inputFile)
        End Sub

        ''' <summary>
        ''' INFO : Verifying if loaded assembly is not nothing.
        ''' </summary>
        Public Function IsAsemblyLoaded()
            Return Not (AssDef Is Nothing)
        End Function

        ''' <summary>
        ''' INFO : Loop through the modules and types of the loaded assembly and start renaming.
        ''' </summary>
        Public Sub RenameAssembly()
            Dim processing As New Cls_Processing(_parameters.RenamingAccept)
            Dim assemblyMainName$ = AssDef.EntryPoint.DeclaringType.Namespace
            For Each modul As ModuleDefinition In AssDef.Modules
                If modul.HasTypes Then
                    For Each type As TypeDefinition In modul.GetAllTypes()
                        RenameSelectedNamespace(type, assemblyMainName, processing)
                    Next
                End If
            Next
        End Sub

        ''' <summary>
        ''' INFO : Rename the main Namespace or all namespaces according to Cls_Parameters.RenameMainNamespaceSetting setting.
        ''' </summary>
        ''' <param name="type"></param>
        ''' <param name="assemblyMainName"></param>
        ''' <param name="processing"></param>
        Private Sub RenameSelectedNamespace(type As TypeDefinition, assemblyMainName$, processing As Cls_Processing)
            If _parameters.RenamingAccept.RenameMainNamespaceSetting = CBool(Cls_RenamerState.RenameMainNamespace.Only) Then
                If type.Namespace.StartsWith(assemblyMainName) Then
                    processing.ProcessType(type)
                End If
            Else
                processing.ProcessType(type)
            End If
        End Sub

        ''' <summary>
        ''' INFO : Records changes to the loaded assembly. It uses Mono Cecil library.
        ''' </summary>
        Public Sub WriteAssembly()
            AssDef.Write(_parameters.outputFile)
        End Sub

        ''' <summary>
        ''' INFO : Clear the randomize names from the dictionary.
        ''' </summary>
        Public Sub Clean()
            Cls_Randomizer.CleanUp()
            Cls_Referencer.CleanUp()
            Cls_Mapping.CleanUp()
        End Sub
#End Region

    End Class
End Namespace
