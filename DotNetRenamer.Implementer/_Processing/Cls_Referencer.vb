﻿Imports Mono.Cecil
Imports Mono.Cecil.Cil

Namespace Processing

    ''' <summary>
    ''' INFO : This is used by the Cls_Processing class. 
    '''        This will updates all the references of the selected assembly.
    ''' </summary>
    Friend NotInheritable Class Cls_Referencer

#Region " Variables "
        Private Shared _MethodReferences As New List(Of MethodReference)()
#End Region

#Region " Methods "

        ''' <summary>
        ''' INFO : Return Method references from typeDef argument.
        ''' </summary>
        ''' <param name="TypeDef"></param>
        Friend Shared Function GetMethodReferences(TypeDef As TypeDefinition) As List(Of MethodReference)
            _MethodReferences.Clear()
            For Each def In TypeDef.Methods
                If def.HasBody Then
                    If def.Body.Instructions.Count <> 0 Then
                        For Each InStruct As Cil.Instruction In def.Body.Instructions
                            If InStruct.OpCode.OperandType <> Mono.Cecil.Cil.OperandType.InlineMethod Then
                                Continue For
                            End If
                            _MethodReferences.Add(DirectCast(InStruct.Operand, MethodReference))
                        Next
                    End If
                End If
            Next
            Return _MethodReferences
        End Function

        ''' <summary>
        ''' INFO : Update Method References.
        ''' </summary>
        ''' <remarks>
        ''' REMARKS : Special thanks to NCloak Obfuscator open source code.
        ''' </remarks>
        ''' <param name="mDef"></param>
        Friend Shared Sub UpdateMethodRef(mDef As MethodDefinition)
            If mDef.HasBody Then
                For Each ins As Instruction In mDef.Body.Instructions
                    Select Case ins.OpCode.Name
                        Case "call", "callvirt", "newobj"
                            If TypeOf ins.Operand Is GenericInstanceMethod Then
                                Dim gInstanceM As GenericInstanceMethod = TryCast(ins.Operand, GenericInstanceMethod)
                                UpdateMemberTypeRef(gInstanceM)
                                For Each tr As TypeReference In gInstanceM.GenericArguments
                                    UpdateTypeRef(tr)
                                Next
                            ElseIf TypeOf ins.Operand Is MethodReference Then
                                Dim mRef As MethodReference = TryCast(ins.Operand, MethodReference)
                                If Not mRef.HasParameters Then
                                    UpdateMemberTypeRef(mRef)
                                End If
                            End If
                            Exit Select
                        Case "stfld", "ldfld"
                            Dim fRef As FieldReference = TryCast(ins.Operand, FieldReference)
                            UpdateMemberTypeRef(fRef)
                            Exit Select
                    End Select
                Next
            End If
        End Sub

        ''' <summary>
        ''' INFO : Update MemberType References.      
        ''' </summary>
        ''' <remarks>
        ''' REMARKS : Special thanks to NCloak Obfuscator open source code.
        ''' </remarks>
        ''' <param name="mRef"></param> 
        Friend Shared Sub UpdateMemberTypeRef(mRef As MemberReference)
            Dim methodType As TypeReference = mRef.DeclaringType
            methodType = methodType.Resolve()
            If TypeOf mRef Is MethodSpecification Then
                Dim mSpecific As MethodSpecification = TryCast(mRef, MethodSpecification)
                If Not mSpecific Is Nothing Then
                    Dim meth As MethodReference = mSpecific.GetElementMethod()
                    meth.Name = meth.Resolve().Name
                End If
            ElseIf TypeOf mRef Is FieldReference Then
                Dim fr As FieldReference = TryCast(mRef, FieldReference)
                mRef.Name = fr.Resolve().Name
            ElseIf TypeOf mRef Is MethodReference Then
                Dim mr As MethodReference = TryCast(mRef, MethodReference)
                mRef.Name = mr.Resolve().Name
            End If
        End Sub

        ''' <summary>
        ''' INFO : Update Type References.
        ''' </summary>
        ''' <remarks>
        ''' REMARKS : Special thanks to NCloak Obfuscator open source code.
        ''' </remarks>
        ''' <param name="tRef"></param>
        Friend Shared Sub UpdateTypeRef(tRef As TypeReference)
            If TypeOf tRef.Scope Is AssemblyNameReference Then
                tRef.Name = tRef.Resolve().Name
            End If
            'If TypeOf tRef.Scope Is AssemblyNameReference Then
            '    Dim scope As AssemblyNameReference = TryCast(tRef.Scope, AssemblyNameReference)
            '    If (type.Module.Assembly.Name.Name = scope.Name) Then
            '        tRef.Scope = type.Module.Assembly.Name
            '    End If
            'End If
        End Sub

        ''' <summary>
        ''' INFO : CleanUp Namespaces dictionary and MethodReferences List.
        ''' </summary>
        Friend Shared Sub CleanUp()
            If Not _MethodReferences.Count <> 0 Then _MethodReferences.Clear()
        End Sub

#End Region

    End Class
End Namespace