﻿Imports Mono.Cecil
Imports Mono.Cecil.Cil
Imports DotNetRenamer.Implementer.Context
Imports DotNetRenamer.Helper.RandomizeHelper

Namespace Processing
    ''' <summary>
    ''' INFO : This is the forth step of the renamer library. 
    '''        This is the core of the rename library !
    ''' </summary>
    Friend NotInheritable Class Cls_Renamer

#Region " Methods "
        ''' <summary>
        ''' INFO : Rename the method. Return methodDefinition member.
        ''' </summary>
        ''' <param name="method"></param>
        Friend Shared Function RenameMethod(type As TypeDefinition, method As MethodDefinition) As MethodDefinition
            Dim MethodOriginal$ = method.Name
            Dim MethodPublicObf$ = Cls_Randomizer.GenerateNew()

            For Each reference As MethodReference In Cls_Referencer.GetMethodReferences(method.DeclaringType)
                If ((Not (reference.DeclaringType.Name = type.Name) OrElse Not _
                     (reference.DeclaringType.Namespace = type.Namespace)) OrElse _
                    ((((Object.ReferenceEquals(reference, method) OrElse Not _
                        (reference.Name = MethodOriginal)) OrElse _
                    ((reference.HasThis <> method.HasThis) OrElse _
                     (reference.CallingConvention <> method.CallingConvention))) OrElse _
                   (((reference.Parameters.Count <> method.Parameters.Count) OrElse _
                     (reference.GenericParameters.Count <> method.GenericParameters.Count)) OrElse Not _
                 (reference.ReturnType.Name = method.ReturnType.Name))) OrElse Not _
                   (reference.ReturnType.Namespace = method.ReturnType.Namespace))) Then
                    Continue For
                End If
                Dim flag As Boolean = True
                Dim num% = 0
                Do While (num < method.Parameters.Count)
                    If (reference.Parameters.Item(num).ParameterType.FullName <> method.Parameters.Item(num).ParameterType.FullName) Then
                        flag = False
                        Exit Do
                    End If
                    num += 1
                Loop
                For num = 0 To method.GenericParameters.Count - 1
                    If (reference.GenericParameters.Item(num).FullName <> method.GenericParameters.Item(num).FullName) Then
                        flag = False
                        Exit For
                    End If
                Next num

                If flag Then
                    reference.Name = MethodPublicObf
                End If
            Next
            If method.IsPInvokeImpl Then
                If method.PInvokeInfo.EntryPoint = String.Empty Then method.PInvokeInfo.EntryPoint = MethodOriginal
            End If

            method.Name = Cls_Mapping.RenameMethodMember(method, MethodPublicObf)

            Return method
        End Function

        ''' <summary>
        ''' INFO : Rename Parameters from method.
        ''' </summary>
        ''' <param name="method"></param>
        Friend Shared Sub RenameParameters(method As MethodDefinition)
            If method.HasParameters Then
                For Each ParDef As ParameterDefinition In method.Parameters
                    If ParDef.CustomAttributes.Count = 0 Then
                        ParDef.Name = Cls_Mapping.RenameParamMember(ParDef, Cls_Randomizer.GenerateNew())
                    End If
                Next
            End If
            If method.HasGenericParameters Then
                For Each GenPar As GenericParameter In method.GenericParameters
                    If GenPar.CustomAttributes.Count = 0 Then
                        GenPar.Name = Cls_Mapping.RenameGenericParamMember(GenPar, Cls_Randomizer.GenerateNew())
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' INFO : Rename Variables from method.
        ''' </summary>
        ''' <param name="method"></param>
        Friend Shared Sub RenameVariables(Method As MethodDefinition)
            If Method.HasBody Then
                For Each vari As VariableDefinition In Method.Body.Variables
                    vari.Name = Cls_Mapping.RenameVariableMember(vari, Cls_Randomizer.GenerateNew())
                Next
            End If
        End Sub

        ''' <summary>
        ''' INFO : Rename embedded Resources from Resources dir and updates method bodies.
        ''' </summary>
        ''' <param name="TypeDef"></param>
        ''' <param name="NamespaceOriginal"></param>
        ''' <param name="NamespaceObfuscated"></param>
        ''' <param name="TypeOriginal"></param>
        ''' <param name="TypeObfuscated"></param>
        Friend Shared Sub RenameResources(TypeDef As TypeDefinition, ByRef NamespaceOriginal$, ByRef NamespaceObfuscated$, TypeOriginal$, TypeObfuscated$)
            Dim ModuleDef As ModuleDefinition = TypeDef.Module
            For Each EmbRes As EmbeddedResource In ModuleDef.Resources
                If EmbRes.Name = NamespaceOriginal & "." & TypeOriginal & ".resources" Then
                    EmbRes.Name = If(NamespaceObfuscated = String.Empty, TypeObfuscated & ".resources", NamespaceObfuscated & "." & TypeObfuscated & ".resources")
                End If
            Next

            If TypeDef.HasMethods Then
                For Each method In TypeDef.Methods
                    If method.HasBody Then
                        For Each inst In method.Body.Instructions
                            If inst.OpCode = OpCodes.Ldstr Then
                                If inst.Operand.ToString() = (NamespaceOriginal & "." & TypeOriginal) Then
                                    inst.Operand = If(NamespaceObfuscated = String.Empty, TypeObfuscated, NamespaceObfuscated & "." & TypeObfuscated)
                                End If
                            End If
                        Next
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' INFO : Rename embedded Resources from Resources dir and from ResourcesManager method.
        ''' </summary>
        ''' <param name="typeDef"></param>
        Friend Shared Sub RenameResourceManager(typeDef As TypeDefinition)
            For Each pr In typeDef.Properties
                If Not pr.GetMethod Is Nothing Then
                    If pr.GetMethod.Name = "get_ResourceManager" Then
                        If pr.GetMethod.HasBody Then
                            Dim instruction As Cil.Instruction
                            If pr.GetMethod.Body.Instructions.Count <> 0 Then
                                For Each instruction In pr.GetMethod.Body.Instructions
                                    If TypeOf instruction.Operand Is String Then
                                        Dim NewResManagerName$ = instruction.Operand
                                        For Each EmbRes As EmbeddedResource In typeDef.Module.Resources
                                            If EmbRes.Name = instruction.Operand & ".resources" Then
                                                NewResManagerName = Cls_Randomizer.GenerateNew()
                                                EmbRes.Name = NewResManagerName & ".resources"
                                            End If
                                        Next
                                        instruction.Operand = NewResManagerName
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' INFO : Rename Property.
        ''' </summary>
        ''' <param name="prop"></param>
        ''' <param name="obfuscatedN"></param>
        Friend Shared Sub RenameProperty(prop As PropertyDefinition, obfuscatedN$)
            prop.Name = Cls_Mapping.RenamePropertyMember(prop, obfuscatedN)
            If Not prop.GetMethod Is Nothing Then
                prop.GetMethod.Name = Cls_Mapping.RenameMethodMember(prop.GetMethod, Cls_Randomizer.GenerateNew())
            End If
            If Not prop.SetMethod Is Nothing Then
                prop.SetMethod.Name = Cls_Mapping.RenameMethodMember(prop.SetMethod, Cls_Randomizer.GenerateNew())
            End If
        End Sub

        ''' <summary>
        ''' INFO : Rename Field.
        ''' </summary>
        ''' <param name="field"></param>
        ''' <param name="obfuscatedN"></param>
        Friend Shared Sub RenameField(field As FieldDefinition, obfuscatedN$)
            field.Name = Cls_Mapping.RenameFieldMember(field, obfuscatedN)
        End Sub

        ''' <summary>
        ''' INFO : Rename Event.
        ''' </summary>
        ''' <param name="events"></param>
        ''' <param name="obfuscatedN"></param>
        Friend Shared Sub RenameEvent(events As EventDefinition, obfuscatedN$)
            events.Name = Cls_Mapping.RenameEventMember(events, obfuscatedN)
        End Sub

        ''' <summary>
        ''' INFO : Rename CustomAttributes.
        ''' </summary>
        ''' <remarks>
        ''' REMARKS : Only AccessedThroughPropertyAttribute attribute is renamed to prevent de4Dot to retrieve original names.
        ''' </remarks>
        ''' <param name="type"></param>
        ''' <param name="prop"></param>
        ''' <param name="originalN"></param>
        ''' <param name="obfuscatedN"></param> 
        Friend Shared Sub RenameCustomAttributes(type As TypeDefinition, prop As PropertyDefinition, originalN$, obfuscatedN$)
            If type.HasFields Then
                For Each field As FieldDefinition In type.Fields
                    If field.IsPrivate Then
                        If field.HasCustomAttributes Then
                            For Each ca In field.CustomAttributes
                                If ca.AttributeType.Name = "AccessedThroughPropertyAttribute" Then
                                    If ca.HasConstructorArguments Then
                                        If ca.ConstructorArguments(0).Value = originalN Then
                                            ca.ConstructorArguments(0) = New CustomAttributeArgument(ca.AttributeType, obfuscatedN)
                                            RenameProperty(prop, obfuscatedN)
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next
            End If
            RenameCustomAttributesValues(prop)
        End Sub

        Friend Shared Sub RenameCustomAttributesValues(member As Object)
            If member.HasCustomAttributes Then
                For Each ca As CustomAttribute In member.CustomAttributes
                    If Not ca Is Nothing Then
                        If ca.AttributeType.Name = "CategoryAttribute" OrElse ca.AttributeType.Name = "DescriptionAttribute" Then
                            If ca.HasConstructorArguments Then
                                ca.ConstructorArguments(0) = New CustomAttributeArgument(ca.AttributeType, Cls_Randomizer.GenerateNew())
                            End If
                        End If
                    End If
                Next
            End If
        End Sub

#End Region

    End Class
End Namespace
