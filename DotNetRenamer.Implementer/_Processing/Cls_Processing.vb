﻿Imports Mono.Cecil
Imports DotNetRenamer.Implementer.Context
Imports DotNetRenamer.Helper.CecilHelper
Imports DotNetRenamer.Helper.RandomizeHelper

Namespace Processing

    ''' <summary>
    ''' INFO : This is the third step of the renamer library. 
    '''        This will process to rename types and members from the selected assembly with settings of your choice.
    ''' </summary>
    Public Class Cls_Processing

#Region " Variables "
        ' Renaming State
        Private _Namespaces As Boolean
        Private _Types As Boolean
        Private _Methods As Boolean
        Private _Properties As Boolean
        Private _Fields As Boolean
        Private _CustomAttributes As Boolean
        Private _Events As Boolean
        Private _Variables As Boolean
        Private _Parameters As Boolean
        ' Replace Namespaces by empty.string
        Private _EmptyNamespace As Boolean
        ' Store My.Resources Namespaces
        Private _MyResourcesNamespace As String = String.Empty

#End Region

#Region " Initialize "
        ''' <summary>
        ''' INFO : Initializes a new instance of the Processing.Cls_Processing class from which is started the task of renaming.
        ''' </summary>
        ''' <param name="RenamingAccept"></param>
        Public Sub New(RenamingAccept As Cls_RenamerState)
            If RenamingAccept.RenameRuleSetting = Cls_RenamerState.RenameRule.Full Then
                _Namespaces = True
                _Types = True
                _Methods = True
                _Properties = True
                _Fields = True
                _CustomAttributes = True
                _Events = True
                _Variables = True
                _Parameters = True
                _EmptyNamespace = True
                Cls_RandomizerType.RenameSetting = Cls_RandomizerType.RenameEnum.Alphabetic
            ElseIf RenamingAccept.RenameRuleSetting = Cls_RenamerState.RenameRule.Medium Then
                _Namespaces = True
                _Types = True
                _Methods = True
                _Properties = True
                _Fields = True
                _CustomAttributes = True
                _Events = False
                _Variables = False
                _Parameters = False
                _EmptyNamespace = False
                Cls_RandomizerType.RenameSetting = Cls_RandomizerType.RenameEnum.Japanese
            Else
                _Namespaces = RenamingAccept.Namespaces
                _Types = RenamingAccept.Types
                _Methods = RenamingAccept.Methods
                _Properties = RenamingAccept.Properties
                _Fields = RenamingAccept.Fields
                _CustomAttributes = RenamingAccept.CustomAttributes
                _Events = RenamingAccept.Events
                _Variables = RenamingAccept.Variables
                _Parameters = RenamingAccept.Parameters
                _EmptyNamespace = CBool(RenamingAccept.ReplaceNamespacesSetting)
                Cls_RandomizerType.RenameSetting = RenamingAccept.RenamingType
            End If
        End Sub
#End Region

#Region " Methods "

        ''' <summary>
        ''' INFO : This is the EntryPoint of the renamer method ! Namespaces, Types and Resources renaming.
        ''' </summary>
        ''' <param name="type"></param>
        Public Sub ProcessType(type As TypeDefinition)

            Dim NamespaceOriginal$ = type.Namespace
            Dim NamespaceObfuscated$ = type.Namespace

            If Not type.Name = "<Module>" Then
                If _Namespaces Then
                    NamespaceObfuscated = If(_EmptyNamespace = True, String.Empty, Cls_Randomizer.GenerateNew())
                    If type.Namespace.EndsWith(".My.Resources") Then _MyResourcesNamespace = NamespaceObfuscated
                    type.Namespace = Cls_Mapping.RenameTypeDef(type, NamespaceObfuscated, True)
                End If
            End If

            If Cls_CecilHelper.IsRenamable(type) Then
                Dim TypeOriginal$ = type.Name
                If _CustomAttributes Then
                    Cls_Renamer.RenameCustomAttributesValues(type)
                End If
                If _Types Then
                    type.Name = Cls_Mapping.RenameTypeDef(type, Cls_Randomizer.GenerateNew())
                    Cls_Renamer.RenameResources(type, NamespaceOriginal, NamespaceObfuscated, TypeOriginal, type.Name)
                End If

                If _Namespaces Then
                    type.Namespace = Cls_Mapping.RenameTypeDef(type, NamespaceObfuscated, True)
                    Cls_Renamer.RenameResources(type, NamespaceOriginal, NamespaceObfuscated, TypeOriginal, TypeOriginal)
                End If

                If type.HasProperties Then
                    If type.Namespace = _MyResourcesNamespace Then
                        Cls_Renamer.RenameResourceManager(type)
                    End If
                End If
            End If

            If _Methods OrElse _Variables OrElse _Parameters Then
                If type.HasMethods Then
                    ProcessMethods(type)
                End If
            End If

            If _Properties OrElse _CustomAttributes Then
                If type.HasProperties Then
                    ProcessProperties(type)
                End If
            End If

            If _Fields Then
                If type.HasFields Then
                    ProcessFields(type)
                End If
            End If

            If _Events OrElse _CustomAttributes Then
                If type.HasEvents Then
                    ProcesEvents(type)
                End If
            End If
        End Sub

        ''' <summary>
        ''' INFO : Methods, Parameters and Variables renamer routine.
        ''' </summary>
        ''' <param name="type"></param>
        Private Sub ProcessMethods(type As TypeDefinition)
            For Each method As MethodDefinition In type.Methods
                If Cls_CecilHelper.IsRenamable(method) Then
                    Dim meth As MethodDefinition = method
                    If _Methods Then
                        meth = Cls_Renamer.RenameMethod(type, meth)
                    End If
                    If _Parameters Then
                        Cls_Renamer.RenameParameters(meth)
                    End If
                    If _Variables Then
                        Cls_Renamer.RenameVariables(meth)
                    End If
                Else
                    If _Parameters Then
                        Cls_Renamer.RenameParameters(method)
                    End If
                    If _Variables Then
                        Cls_Renamer.RenameVariables(method)
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' INFO : Properties, CustomAttributes (Only "AccessedThroughPropertyAttribute" attribute) renamer routine. 
        ''' </summary>
        ''' <param name="type"></param>
        Private Sub ProcessProperties(type As TypeDefinition)
            For Each propDef As PropertyDefinition In type.Properties
                If Cls_CecilHelper.IsRenamable(propDef) Then

                    Dim originalN$ = propDef.Name
                    Dim obfuscatedN$ = Cls_Randomizer.GenerateNew()

                    propDef.Name = obfuscatedN

                    If _CustomAttributes Then
                        Cls_Renamer.RenameCustomAttributes(type, propDef, originalN, obfuscatedN)
                    End If

                    If _Properties Then
                        If propDef.GetMethod IsNot Nothing Then
                            Cls_Referencer.UpdateMethodRef(propDef.GetMethod)
                        End If
                    End If

                    If _CustomAttributes Then
                        Cls_Renamer.RenameCustomAttributes(type, propDef, originalN, obfuscatedN)
                    End If

                    If _Properties Then
                        If propDef.SetMethod IsNot Nothing Then
                            Cls_Referencer.UpdateMethodRef(propDef.SetMethod)
                        End If
                    End If
                End If
            Next
        End Sub

        ''' <summary>
        ''' INFO : Fields renamer routine. 
        ''' </summary>
        ''' <param name="type"></param>
        Private Sub ProcessFields(type As TypeDefinition)
            For Each field As FieldDefinition In type.Fields
                If Cls_CecilHelper.IsRenamable(field) Then
                    Cls_Renamer.RenameField(field, Cls_Randomizer.GenerateNew())
                End If
            Next
        End Sub

        ''' <summary>
        ''' INFO : Events renamer routine. 
        ''' </summary>
        ''' <param name="type"></param>
        Private Sub ProcesEvents(type As TypeDefinition)
            For Each events As EventDefinition In type.Events
                If Cls_CecilHelper.IsRenamable(events) Then
                    If _CustomAttributes Then
                        Cls_Renamer.RenameCustomAttributesValues(events)
                    End If
                    If _Events Then
                        Cls_Renamer.RenameEvent(events, Cls_Randomizer.GenerateNew())
                    End If
                End If
            Next
        End Sub


#End Region

    End Class
End Namespace