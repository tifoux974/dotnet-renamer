# **DotNet Renamer** #

# Description

DNR is a open source obfuscator/Renamer which use MonoCecil library for .NET applications !
This project is based on the [DotNet Patcher](http://3dotdevcoder.blogspot.fr/2014/04/dotnet-patcher.html)'s renamer library 

You can use these 2 libraries separately without the DotNetRenamer GUI

1. **Core20Reader** : a simple PE DotNet Parser/Reader
2. **The renamer based on** : Core20Reader.dll, DotNetRenamer.Helper.dll, DotNetRenamer.Implementer.dll

# Screenshot

![DotNetRenamer.png](https://bitbucket.org/repo/egqzed/images/165358733-DotNetRenamer.png)

![tmp4F24.png](https://bitbucket.org/repo/egqzed/images/1854744963-tmp4F24.png)


# Features

* Doesn't support WPF .exe !
* English UI language only
* Displays selected .exe informations (assembly name, Version, TargetRuntime, TargetCPU, SubSystemType)
* Selecting presets : Full, Medium, Customize
* Selecting encoding chars type : Alphabetic, Dots, Invisible, Chinese, Japanese
* Renaming : Namespaces, Types, Methods, Properties, Fields, Custom Attributes, Events, Parameters, ..... 
* Displays number of renamed members


# Prerequisites

* Compatible VS2012 EDI
* All Windows OS
* DotNet Framework 4.0
* The binary doesn't require installation


# WebSite

* [http://3dotdevcoder.blogspot.fr/](http://3dotdevcoder.blogspot.fr/)


# Credits

* jbevains : for [MonoCecil](https://github.com/jbevain/cecil) library
* Xertz : for his [Login GDI+ theme](http://xertzproductions.weebly.com/login-gdi-theme.html) which I modified a little bit
* Paupino : for some useful functions from his open source project : [NCloak](https://github.com/paupino/ncloak/)


# Copyright

Copyright © 3DotDev 2008-2014


# Licence

[MIT/X11](http://en.wikipedia.org/wiki/MIT_License)